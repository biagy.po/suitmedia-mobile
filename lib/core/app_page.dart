import 'package:get/get.dart';
import 'package:test_suitmedia/screen/auth/login_screen.dart';
import 'package:test_suitmedia/screen/pages/home_screen.dart';
import 'package:test_suitmedia/screen/pages/user_screen.dart';
import 'package:test_suitmedia/screen/pages/webview_screen.dart';
import 'app_routes.dart';

abstract class AppPages {
  static const initial = Routes.LOGIN;
  static final routes = [
    GetPage(name: Routes.LOGIN, page: () => LoginScreen()),
    GetPage(name: Routes.HOME, page: () => HomeScreen()),
    GetPage(name: Routes.USER, page: () => UserScreen()),
    GetPage(name: Routes.WEBVIEW, page: () => const WebViewScreen()),
  ];
}
