abstract class Routes {
  // ignore: constant_identifier_names
  static const String LOGIN = '/login_screen';
  // ignore: constant_identifier_names
  static const String HOME = '/home_screen';
  // ignore: constant_identifier_names
  static const String USER = '/user_screen';
  // ignore: constant_identifier_names
  static const String WEBVIEW = '/webview_screen';
}
