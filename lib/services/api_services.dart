import 'package:test_suitmedia/models/user_model.dart';
import 'package:test_suitmedia/utilities/helpers/api_base_helper.dart';
import 'package:test_suitmedia/utilities/helpers/base_error_helper.dart';
import '../utilities/constants.dart' as constants;

class ApiServices with BaseErrorHelper {
  final _helper = ApiBaseHelper();

  Future<UserModel?> serviceGetUser() async {
    var response = await _helper
        .get(
          url: constants.URL_GET_USERS,
        )
        .catchError(handleError);
    if (response != null) {
      return UserModel.fromJson(response);
    }
  }
}
