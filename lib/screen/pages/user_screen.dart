import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_suitmedia/controller/user_controller.dart';

class UserScreen extends StatelessWidget {
  UserScreen({Key? key}) : super(key: key);

  final controller = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Users',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
          ),
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              )),
          elevation: 1,
        ),
        body: Obx(
          () => Container(
              height: height,
              color: Colors.white,
              child: controller.isLoad.value
                  ? const Center(
                      child: CircularProgressIndicator(color: Colors.black))
                  : controller.userModel.value.data!.isEmpty
                      ? const Center(
                          child: Text('Tidak ada data'),
                        )
                      : ListView.builder(
                          padding: const EdgeInsets.all(24),
                          itemCount: controller.userModel.value.data!.length,
                          itemBuilder: (context, i) {
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: InkWell(
                                onTap: () {
                                  Get.delete<UserController>();
                                  Get.offNamedUntil(
                                      'home_screen', (route) => false,
                                      parameters: {
                                        'name':
                                            '${controller.userModel.value.data![i].firstName} ${controller.userModel.value.data![i].lastName}',
                                        'email':
                                            '${controller.userModel.value.data![i].email}',
                                        'avatar':
                                            '${controller.userModel.value.data![i].avatar}',
                                      });
                                },
                                child: ListTile(
                                  leading: ClipRRect(
                                    borderRadius: BorderRadius.circular(30.0),
                                    child: Image.network(
                                      '${controller.userModel.value.data![i].avatar}',
                                    ),
                                  ),
                                  title: Text(
                                    '${controller.userModel.value.data![i].firstName} ${controller.userModel.value.data![i].lastName}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  subtitle: Text(
                                    '${controller.userModel.value.data![i].email}',
                                    style: const TextStyle(
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          })),
        ));
  }
}
