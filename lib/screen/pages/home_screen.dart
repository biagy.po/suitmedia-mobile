import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_suitmedia/controller/home_controller.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  final controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Home',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
        ),
        leading: IconButton(
            onPressed: () {
              Get.delete<HomeController>();
              Get.offNamedUntil('login_screen', (route) => false);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            )),
        elevation: 1,
      ),
      body: Obx(() => Container(
            height: height,
            color: Colors.white,
            child: ListView(
              padding: const EdgeInsets.all(24.0),
              children: [
                // welcome user
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  const Text('Welcome'),
                  Text(
                    controller.name.value,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w600),
                  )
                ]),
                // User Profile
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: height / 6,
                    ),
                    SizedBox(
                        height: 160,
                        width: 160,
                        child: controller.avatar.value.isNotEmpty
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(120.0),
                                child: Image.network(
                                  controller.avatar.value,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Image.asset(
                                'assets/empty_image.png',
                                fit: BoxFit.cover,
                              )),
                    const SizedBox(
                      height: 50,
                    ),
                    controller.isData.value
                        ? Column(
                            children: [
                              Text(
                                controller.name.value,
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                controller.email.value,
                                style: const TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              InkWell(
                                onTap: () {
                                  Get.toNamed('webview_screen');
                                },
                                child: const Text(
                                  'website',
                                  style: TextStyle(
                                      fontSize: 15,
                                      decoration: TextDecoration.underline),
                                ),
                              ),
                            ],
                          )
                        : const Text(
                            'Choose a User to show the profile',
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          )
                  ],
                )
              ],
            ),
          )),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        height: 75,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              height: 50,
              minWidth: 300,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              onPressed: () {
                Get.delete<HomeController>();
                Get.toNamed('user_screen');
              },
              child: const SizedBox(
                height: 45,
                child: Center(
                  child: Text("Choose a User",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w600)),
                ),
              ),
              color: const Color(0xFF3366FF),
            ),
          ],
        ),
      ),
    );
  }
}
