import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:test_suitmedia/controller/auth/login_controller.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
          body: Stack(
        children: [
          ColorFiltered(
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.75), BlendMode.dstATop),
            child: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF3366FF),
                      Color(0xFF00CCFF),
                    ],
                    begin: FractionalOffset(0.0, 0.0),
                    end: FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
            ),
          ),
          Form(
            key: controller.formKey,
            child: ListView(
              padding: const EdgeInsets.all(32.0),
              children: [
                SizedBox(
                  height: height / 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      child: const Icon(
                        Icons.person_add,
                        size: 40,
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: Color(0xFFe0f2f1)),
                    ),
                    const SizedBox(height: 50),
                    TextFormField(
                      controller: controller.nameController,
                      autofocus: false,
                      style: const TextStyle(
                        fontSize: 18.0,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Name',
                        contentPadding: const EdgeInsets.only(
                            left: 24.0, bottom: 8.0, top: 8.0),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      validator: (nameValue) {
                        if (nameValue!.isEmpty) {
                          return 'Masukan Name';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 30),
                    TextFormField(
                      controller: controller.palindromeController,
                      autofocus: false,
                      style: const TextStyle(
                        fontSize: 18.0,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Panlindrome',
                        contentPadding: const EdgeInsets.only(
                            left: 24.0, bottom: 8.0, top: 8.0),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      validator: (panlindromeValue) {
                        if (panlindromeValue!.isEmpty) {
                          return 'Masukan Panlindrome';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 40),
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      onPressed: () {
                        controller.checkPalindrome(context);
                      },
                      child: const SizedBox(
                        height: 45,
                        child: Center(
                          child: Text("CHECK",
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      color: const Color(0xFF3366FF),
                    ),
                    const SizedBox(height: 20),
                    MaterialButton(
                      onPressed: () {
                        Get.toNamed('/home_screen');
                      },
                      child: const SizedBox(
                        height: 45,
                        child: Center(
                          child: Text("NEXT",
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      color: const Color(0xFF3366FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
