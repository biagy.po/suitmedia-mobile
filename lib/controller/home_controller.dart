import 'package:get/get.dart';

class HomeController extends GetxController {
  final data = Get.parameters;
  var name = ''.obs;
  var email = ''.obs;
  var avatar = ''.obs;
  var isData = false.obs;

  void checkData() {
    if (data.isNotEmpty) {
      name.value = data['name']!;
      email.value = data['email']!;
      avatar.value = data['avatar']!;
      isData.value = true;
    }
  }

  @override
  void onInit() {
    checkData();
    super.onInit();
  }
}
