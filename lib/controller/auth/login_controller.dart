import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:test_suitmedia/widgets/error_dialog.dart';
import 'package:test_suitmedia/widgets/error_message_snackbar.dart';
import 'package:test_suitmedia/widgets/success_dialog.dart';

class LoginController extends GetxController {
  var nameController = TextEditingController();
  var palindromeController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  void checkPalindrome(BuildContext context) {
    if (formKey.currentState!.validate()) {
      String combineString =
          '${nameController.value.text} ${palindromeController.value.text}';
      if (combineString == combineString.split('').reversed.join()) {
        successDialog(context: context, text: '$combineString is Panlindrome');
      } else {
        errorDialog(
            context: context, text: '$combineString is not Panlindrome');
      }
    } else {
      errorMessageSnackbar('Form tidak boleh kosong!');
    }
  }
}
