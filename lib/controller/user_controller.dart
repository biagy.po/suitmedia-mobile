import 'package:get/get.dart';
import 'package:test_suitmedia/models/user_model.dart';
import 'package:test_suitmedia/services/api_services.dart';

class UserController extends GetxController {
  final _apiService = ApiServices();
  var userModel = UserModel().obs;
  final isLoad = false.obs;

  Future<void> getUser() async {
    isLoad.value = true;
    var data = await _apiService.serviceGetUser();
    if (data != null) {
      userModel.value = UserModel();
      isLoad.value = false;
      userModel.value = data;
    } else {
      isLoad.value = false;
    }
  }

  @override
  void onInit() {
    getUser();
    super.onInit();
  }
}
