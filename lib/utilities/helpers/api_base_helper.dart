import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'api_exception.dart';

class ApiBaseHelper {
  static var client = http.Client();
  // ignore: constant_identifier_names
  static const int TIME_OUT_DURATION = 20;

  Future<dynamic> get({var url, var headers}) async {
    try {
      final response = await http
          .get(Uri.parse(url), headers: headers)
          .timeout(const Duration(seconds: TIME_OUT_DURATION));
      return _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    } on TimeoutException {
      throw ApiNotRespondingException('API not responded in time');
    }
  }

  Future<dynamic> post({var url, var headers, var body}) async {
    try {
      final response = await http
          .post(Uri.parse(url), headers: headers, body: body)
          .timeout(const Duration(seconds: TIME_OUT_DURATION));

      return _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    } on TimeoutException {
      throw ApiNotRespondingException(
          'API not responded in time', url.toString());
    }
  }
}

dynamic _returnResponse(http.Response response) {
  switch (response.statusCode) {
    case 200:
      var responJson = json.decode(response.body.toString());
      return responJson;
    case 201:
      var responJson = json.decode(response.body.toString());
      return responJson;
    case 400:
      throw BadRequestException(json.decode(response.body.toString()));
    case 401:
    case 403:
      throw UnAuthorizedException(json.decode(response.body.toString()));
    case 500:
    default:
      throw SomethingDataException(
          'Error occured with code : ${response.body}');
  }
}
