import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

successDialog({
  required BuildContext context,
  required String text
  // required Function() onTapOk,
}) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.SUCCES,
    animType: AnimType.TOPSLIDE,
    desc: text,
    btnOkColor: Colors.green,
    btnOkText: 'Oke',
    btnOkOnPress: () {},
    // btnOkColor: ColorsApp.primaryColor,
    // btnOkOnPress: onTapOk,
    dismissOnBackKeyPress: true,
  ).show();
}
